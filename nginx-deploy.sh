#!/bin/sh
#########################
# 4S Nginx deployment script
#
# This script creates nginx configuration for a given server url and deploys it

#Log File
LOG=~/logs/nginx-setup.log

display_usage() {
  echo "Usage: $0 [SERVER-URL] [OPTIONAL:SOURCEREPO]"
}

touch_log_file() { 
  mkdir -p "$(dirname "$LOG")" || return; touch "$LOG";
}

log() {
  echo "$(date): $*" >> ${LOG}; tail -n1 ${LOG}
}

die() {
  log "Error during deployment, check logfile: $LOG"
  exit 2
}

perform() {
  log "$1"
  eval "$1" >> ${LOG} || die
}

#Check if not enough arguments
if [[  $# -le 0 ]]; then
  display_usage
  exit 1
fi

#Extract domain name from server url.
#The following checks if server url begins with http:// or https://
if [[ ${1,,} =~ ^https?://+ ]]
then
    SERVER_URL="$(echo "$1" | awk -F/ '{print $3}')"
else
    SERVER_URL=$1
fi


#The remote repository 
REPOSRC={$2:-"ssh://git@bitbucket-server.alexandra.dk:7999/s4/nginx.git"}

#The local repository
LOCALREPO=~/src/nginx
LOCALREPO_VC_DIR=$LOCALREPO/.git



touch_log_file
echo "" >> ${LOG}
log "NGINX DEPLOY BEGIN:"

#Clone repo if not present, else pull
if [ ! -d $LOCALREPO_VC_DIR ]
then
    perform "git clone $REPOSRC $LOCALREPO"
    perform "cd $LOCALREPO"
else
    perform "cd $LOCALREPO"
    perform "git pull"
fi


#In place replacement of configuration files, with backup saved with .bak extensions
perform "sed -i.bak \"s+server_url+$SERVER_URL+g\" service.conf"

#Run the docker images
perform "docker-compose -f docker-compose.fluentd-logging.yml -f docker-compose.yml up --force-recreate  -d"

log "NGINX DEPLOY END"

# End