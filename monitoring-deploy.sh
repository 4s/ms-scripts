#!/bin/sh
#########################
# 4S Monitoring deployment script
# 
# This script deploys fluentd, elasticsearch, kibana, elasticsearch-curator and portainer
# harshit.mahapatra@alexandra.dk

#Log File
LOG=~/logs/monitoring-setup.log

display_usage() {
  echo "Usage: $0 [OPTIONAL: Elasticsearch volume mount directory]"
}

touch_log_file() { 
  mkdir -p "$(dirname "$LOG")" || return; touch "$LOG";
}

log() {
  echo "$(date): $*" >> ${LOG}; tail -n1 ${LOG}
}

die() {
  log "Error during deployment, check logfile: $LOG"
  exit 2
}

perform() {
  log "$1"
  eval "$1" >> ${LOG} || die
}


#The remote repository 
REPOSRC=ssh://git@bitbucket-server.alexandra.dk:7999/s4/monitoring.git
#The local repository
LOCALREPO=~/src/monitoring
LOCALREPO_VC_DIR=$LOCALREPO/.git

#The volume mount directory
VOLUME_DIRECTORY=$1

touch_log_file
echo "" >> ${LOG}
log "MONITORING DEPLOY BEGIN:"

#Clone repo if not present, else pull
if [ ! -d $LOCALREPO_VC_DIR ]
then
    perform "git clone $REPOSRC $LOCALREPO"
    perform "cd $LOCALREPO"
else
    perform "cd $LOCALREPO"
    perform "git pull"
fi


if [ -n "$VOLUME_DIRECTORY" ]
then
    log "Deploying logging stack with elasticsearch volume mounted at $VOLUME_DIRECTORY/elasticsearch-data"
    #Build and run fluentd, elasticsearch and kibana with mounted managed elasticsearch volume
    perform "printf 'version: \"3.4\"\nservices:\n  elasticsearch:\n    volumes:\n     - $VOLUME_DIRECTORY/elasticsearch-data:/usr/share/elasticsearch/data\n'> docker-compose.deploy.yml"
    perform "docker-compose -f docker-compose.yml -f docker-compose.portainer.yml -f docker-compose.deploy.yml up --force-recreate --build -d"
else
    log "Deploying logging stack with docker managed elasticsearch volume"
    #Build and run fluentd, elasticsearch and kibana with docker managed elasticsearch volume
    perform "docker-compose -f docker-compose.yml -f docker-compose.portainer.yml up --force-recreate --build -d"
fi

log "MONITORING DEPLOY END"