#!/bin/bash
#########################
# Mariadb Database Restore Script
#
# harshit.mahapatra@alexandra.dk

LOG=~/logs/mariadb-restore.log

touch_log_file() { 
  mkdir -p "$(dirname "$LOG")" || return; touch "$LOG";
}

log() {
  echo "$(date): $*" >> ${LOG}; tail -n1 ${LOG}
}

die() {
  log "Error during deployment, check logfile: $LOG"
  exit 2
}

perform() {
  log "$1"
  eval "$1" >> ${LOG} || die
}

function usage()
{
    echo "Update questionnaire database from dump"
    echo
    echo "$(tput setaf 3)Usage:$(tput sgr 0)"
    echo "update-questionnaire-db.sh [-c|--containername <containername>] [-b|--backupfile <backup-file>] [-d|--databasename <databasename>] [-u|--username <database-username>] [-p|--password <database-password>] [-h|help]"
    echo
    echo "$(tput setaf 3)Options:$(tput sgr 0)"
    echo
    echo "$(tput setaf 3)[-c|--containername <containername>]$(tput sgr 0)"
    echo "  Name of container running the questionnaire database."
    echo
    echo "$(tput setaf 3)-b|--backupfile <backup-file>$(tput sgr 0)"
    echo "  File containing dump of the database"
    echo
    echo "$(tput setaf 3)-d|--databasename <databasename>$(tput sgr 0)"
    echo "  Name of database to backup."
    echo
    echo "$(tput setaf 3)-u|--username <database-username>$(tput sgr 0)"
    echo "  Username for accessing the database."
    echo
    echo "$(tput setaf 3)-p|--password <database-password>$(tput sgr 0)"
    echo "  Password for accessing the database."
    echo
    echo "$(tput setaf 3)[-h|help]$(tput sgr 0)"
    echo "  Print this message."
}

# Read commandline arguments:
while :; do
    case $1 in
        -h|--help)
            usage
            exit 1
            ;;
        -c|--containername)
            if [ -n "$2" ]; then
                CONTAINER_NAME="$2"
                shift
            fi
     	    ;;
        -b|--backupfile)
            if [ -n "$2" ]; then
                BACKUP_FILE="$2"
                shift
            fi
            ;;
        -d|--databasename)
            if [ -n "$2" ]; then
                DATABASE_NAME="$2"
                shift
            fi
            ;;
        -u|--username)
            if [ -n "$2" ]; then
                USERNAME="$2"
                shift
            fi
            ;;
        -p|--password)
            if [ -n "$2" ]; then
                PASSWORD="$2"
                shift
            fi
            ;;
        --)              # End of all options.
            shift
            break
            ;;
        -?*)
            printf 'WARN: Unknown option (ignored): %s\n' "$1" >&2
            ;;
        *)               # Default case: No more options, so break out of the loop.
            break
    esac
    shift # past argument or value
done

if [ -z "$CONTAINER_NAME" ] || [ -z "$BACKUP_FILE" ] || [ -z "$DATABASE_NAME" ] || [ -z "$USERNAME" ] || [ -z "$PASSWORD" ]
then
    usage
    exit 1;
fi

log "CONTAINER_NAME: $CONTAINER_NAME"
log "BACKUP_FILE: $BACKUP_FILE"
log "DATABASE_NAME: $DATABASE_NAME"
log "USERNAME: $USERNAME"

touch_log_file
echo "" >> ${LOG}
log "RESTORE PROCESS BEGIN:"
perform "cat $BACKUP_FILE | docker exec -i $CONTAINER_NAME /usr/bin/mysql -u $USERNAME --password=$PASSWORD $DATABASE_NAME"

log "RESTORE PROCESS SUCCESSFUL."
log "RESTORE PROCESS END"

