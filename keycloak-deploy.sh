#!/bin/sh
#########################
# Keycloak deployment script
#
# This deploys keycloak from a given source repository along with configuration present in the given source repository.

#Log File
LOG=~/logs/keycloak-setup.log

display_usage() {
  echo "Usage: $0 [SOURCEREPO] [BRANCH]"
}

touch_log_file() { 
  mkdir -p "$(dirname "$LOG")" || return; touch "$LOG";
}

log() {
  echo "$(date): $*" >> ${LOG}; tail -n1 ${LOG}
}

die() {
  log "Error during deployment, check logfile: $LOG"
  exit 2
}

perform() {
  log "$1"
  eval "$1" >> ${LOG} || die
}

#Check if not enough arguments
if [[  $# -ne 2 ]]; then
  display_usage
  exit 1
fi

#The remote repository 
REPOSRC=$1

#The local repository
LOCALREPO=~/src/keycloak
LOCALREPO_VC_DIR=$LOCALREPO/.git

#The branch
BRANCH=$2

touch_log_file
echo "" >> ${LOG}
log "KEYCLOAK DEPLOY BEGIN:"

#Clone repo if not present, else pull
if [ ! -d $LOCALREPO_VC_DIR ]
then
    perform "git clone $REPOSRC $LOCALREPO"
    perform "cd $LOCALREPO"
else
    perform "cd $LOCALREPO"
    perform "git pull"
fi

#Git checkout branch
perform "git checkout $BRANCH"

#Run the docker images
perform "docker-compose -f docker-compose.yml -f docker-compose.fluentd-logging.yml up --force-recreate --build -d"

log "KEYCLOAK DEPLOY END"

# End