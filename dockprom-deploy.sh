#!/bin/sh
#########################
# 4S Metrics, health and alert monitoring deployment script
# 
# This script deploys prometheus, alertmanager, nodeexporter, cadadvisor, grafana and caddy
# michael.christensen@alexandra.dk

#Log File
LOG=~/logs/dockprom-setup.log

display_usage() {
  echo "Usage: $0 [ADMIN USER] [ADMIN PASSWORD] [GRAFANA ROOT URL] [OPTIONAL: VOLUME_DIRECTORY]"
}

touch_log_file() { 
  mkdir -p "$(dirname "$LOG")" || return; touch "$LOG";
}

log() {
  echo "$(date): $*" >> ${LOG}; tail -n1 ${LOG}
}

die() {
  log "Error during deployment, check logfile: $LOG"
  exit 2
}

perform() {
  log "$1"
  eval "$1" >> ${LOG} || die
}

if [[  $# -le 3 ]]; then
  display_usage
  exit 1
fi

MY_ADMIN_USER=$1
MY_ADMIN_PASS=$2
MY_ROOT_URL=$3
VOLUME_DIRECTORY=${4:-""}

#The remote repository
REPOSRC=ssh://git@bitbucket-server.alexandra.dk:7999/s4/dockprom.git
#The local repository
LOCALREPO=~/src/dockprom
LOCALREPO_VC_DIR=$LOCALREPO/.git



touch_log_file
echo "" >> ${LOG}
log "DOCKPROM DEPLOY BEGIN:"

#Clone repo if not present, else pull
if [ ! -d $LOCALREPO_VC_DIR ]
then
    perform "git clone $REPOSRC $LOCALREPO"
    perform "cd $LOCALREPO"
else
    perform "cd $LOCALREPO"
    perform "git pull"
fi

#Conditionally generate docker-compose.deploy.yml and run prometheus, alertmanager, nodeexporter, cadadvisor, grafana and caddy
if [ ! -z "$VOLUME_DIRECTORY" ]
then
    log "Deploying services with volumes mounted at $VOLUME_DIRECTORY"
    perform "printf 'version: \"3.4\"\nservices:\n  prometheus:\n    volumes:\n     - $VOLUME_DIRECTORY/prometheus-data:/prometheus\n    user: root\n  grafana:\n    volumes:\n     - $VOLUME_DIRECTORY/grafana-data:/var/lib/grafana\n    user: root\n'> docker-compose.deploy.yml"
    perform "ADMIN_USER=${MY_ADMIN_USER} ADMIN_PASSWORD=${MY_ADMIN_PASS} ROOT_URL=${MY_ROOT_URL} SUB_PATH=true docker-compose -f docker-compose.yml -f docker-compose.deploy.yml -f docker-compose.fluentd-logging.yml up --force-recreate --build -d"
else
    perform "ADMIN_USER=${MY_ADMIN_USER} ADMIN_PASSWORD=${MY_ADMIN_PASS} ROOT_URL=${MY_ROOT_URL} SUB_PATH=true docker-compose -f docker-compose.yml -f docker-compose.fluentd-logging.yml up --force-recreate --build -d"
fi

log "DOCKPROM DEPLOY END"