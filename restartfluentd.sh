#!/bin/sh
#########################
# 4S restart fluentd script
#
# This script restarts fluentd
# harshit.mahapatra@alexandra.dk


#Log File
LOG=~/logs/restartfluentd.log

display_usage() {
  echo "Usage: $0 "
}

touch_log_file() { 
  mkdir -p "$(dirname "$LOG")" || return; touch "$LOG";
}

log() {
  echo "$(date): $*" >> ${LOG}; tail -n1 ${LOG}
}

die() {
  log "Error during restarting fluentd check logfile: $LOG"
  exit 2
}

perform() {
  log "$1"
  eval "$1" >> ${LOG} || die
}


if [ ! -d $LOG ]
then
    touch_log_file
fi

echo "" >> ${LOG}
log "Restarting fluentd:"

set -o errexit # exit script when command fails
set -o pipefail # exit status of the last command that threw a non-zero exit code is returned
set -o nounset # exit when script tries to use undeclared variables

log "Stopping fluentd service..."
perform "docker-compose -f ~/src/monitoring/docker-compose.yml stop fluentd"
log "Starting fluentd service..."
perform "docker-compose -f ~/src/monitoring/docker-compose.yml up -d --no-deps fluentd"