#!/bin/bash
#########################
# Questionnaire Data Extraction Script
#
# harshit.mahapatra@alexandra.dk

LOG=~/logs/questionnaire-data-extraction.log

touch_log_file() { 
  mkdir -p "$(dirname "$LOG")" || return; touch "$LOG";
}

update_suffix_file() { 
  mkdir -p "$(dirname "$SUFFIX_FILE")" || return; echo "$SUFFIX">"$SUFFIX_FILE";
}

log() {
  echo "$(date): $*" >> ${LOG}; tail -n1 ${LOG}
}

die() {
  log "Error during deployment, check logfile: $LOG"
  exit 2
}

perform() {
  log "$1"
  eval "$1" >> ${LOG} || die
}

function usage()
{
    echo "Backup a postgres database"
    echo
    echo "$(tput setaf 3)Usage:$(tput sgr 0)"
    echo "extract-questionnaire-data.sh [-t|--token <backend-token>] [-i|--uuid <questionnaire-uuid>] [-h|help]"
    echo
    echo "$(tput setaf 3)Options:$(tput sgr 0)"
    echo
    echo "$(tput setaf 3)[--token <backend-token>]$(tput sgr 0)"
    echo "  Token to access questionnaire-backend. (Required)"
    echo
    echo "$(tput setaf 3)[-i|--uuid <questionnaire-uuid>]$(tput sgr 0)"
    echo "  UUID of the questionnaire(s) whose data is to be extracted. Atleast one UUID is required."
    echo
    echo "$(tput setaf 3)[-d|--directory <extraction-directory>]$(tput sgr 0)"
    echo "  Directory to store extracted data. (Required)"
    echo
    echo "$(tput setaf 3)[-h|help]$(tput sgr 0)"
    echo "  Print this message."
}

# Read commandline arguments:
while :; do
    case $1 in
        -h|--help)
            usage
            exit 1
            ;;
        -t|--token)
            if [ -n "$2" ]; then
                TOKEN="$2"
                shift
            fi
            ;;
        -i|--uuid)
            if [ -n "$2" ]; then
                UUIDS+=("$2")
                shift
            fi
            ;;
        -d|--directory)
            if [ -n "$2" ]; then
                DIRECTORY="$2"
                shift
            fi
            ;;
        --)              # End of all options.
            shift
            break
            ;;
        -?*)
            printf 'WARN: Unknown option (ignored): %s\n' "$1" >&2
            ;;
        *)               # Default case: No more options, so break out of the loop.
            break
    esac
    shift # past argument or value
done

if [ -z "$TOKEN" ] || [ -z "$DIRECTORY" ]
then
    usage
    exit 1;
fi


log "UUIDs: ${UUIDS[@]}"
log "DIRECTORY: $DIRECTORY"

touch_log_file
echo "" >> ${LOG}
log "EXTRACTION PROCESS BEGIN:"

perform "mkdir -p $DIRECTORY"
perform "cd $DIRECTORY"

# Iterate over uuids and extract answers
for uuid in "${UUIDS[@]}"; do
   perform "docker run --rm --network=opentele3net curlimages/curl:7.71.1 -v  'http://questionnaire-nginx/ws/v1/json/?token=$TOKEN&surveyUuid=$uuid' > $DIRECTORY/$uuid.json"
done

log "EXTRACTION PROCESS SUCCESSFUL"

