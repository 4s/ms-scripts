display_usage() {
  echo "Usage: $0 [IMAGE-REPO] [IMAGE NAME] [SERVICE NAME]"
}

if [[  $# -le 2 ]]; then
  display_usage
  exit 1
fi

REPO=$1
IMAGE_NAME=$2
SERVICE_NAME=$3

mkdir -p ~/src/"$SERVICE_NAME"
cd ~/src/"$SERVICE_NAME" || exit

docker login -u dockerpuller -p OophuePhegohz2ae $REPO
docker pull $REPO/$IMAGE_NAME

printf "version: \"3.4\"\nservices:\n  %s:\n    image: %s\nnetworks:\n  default:\n    external:\n      name: opentele3net\n" "$SERVICE_NAME" "$REPO/$IMAGE_NAME" > docker-compose.yml
docker-compose -f docker-compose.yml up -d