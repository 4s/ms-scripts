#!/bin/bash
#########################
# 4S Deploy script
#
# harshit.mahapatra@alexandra.dk

LOG=~/logs/deploy.log

touch_log_file() { 
  mkdir -p "$(dirname "$LOG")" || return; touch "$LOG";
}

log() {
  echo "$(date): $*" >> ${LOG}; tail -n1 ${LOG}
}

die() {
  log "Error during deployment, check logfile: $LOG"
  exit 2
}

perform() {
  log "$1"
  eval "$1" >> ${LOG} || die
}

function usage()
{
    echo "Deploy 4S services using docker-compose"
    echo
    echo "$(tput setaf 3)Usage:$(tput sgr 0)"
    echo "deploy-service.sh [-i|--imagenamewithtag <imagenamewithtag>] [-m|--imagerepository <imagerepository>] [-u|--imagerepositoryusername <imagerepositoryusername>] [-p|--imagerepositorypassword <imagerepositorypassword>] [-s|--servicename <servicename>] [-d|--deploymentname <deploymentname>] [-r|--revision <revision>] [-g|--gitrepositoryproject <name of project containing git repo>] [-o|--portmapping <portmapping>] [-e|--exposed <service exposed via api or not>] [-n|--deploymentenvironment <deployment environment>] [-v|--volumedirectory <volume directory>] [-c|--volumemapping <mapping of volume inside container>] [-h|help]"
    echo
    echo "$(tput setaf 3)Options:$(tput sgr 0)"
    echo
    echo "$(tput setaf 3)[-i|--imagenamewithtag <imagenamewithtag>]$(tput sgr 0)"
    echo "  Name of docker image to pull."
    echo
    echo "$(tput setaf 3)-m|--imagerepository <imagerepository>$(tput sgr 0)"
    echo "  Name of docker repository to pull the image from, the default is docker.alexandra.dk"
    echo
    echo "$(tput setaf 3)-u|--imagerepositoryusername <imagerepositoryusername>$(tput sgr 0)"
    echo "  Username associated with the docker repository from which the image is to be pulled"
    echo
    echo "$(tput setaf 3)-p|--imagerepositorypassword <imagerepositorypassword>$(tput sgr 0)"
    echo "  Password associated with the docker repository from which the image is to be pulled"
    echo
    echo "$(tput setaf 3)-s|--servicename <servicename>$(tput sgr 0)"
    echo "  Name of the service being deployed."
    echo
    echo "$(tput setaf 3)-d|--deploymentname <deploymentname>$(tput sgr 0)"
    echo "  Name of the deployment."
    echo
    echo "$(tput setaf 3)-r|--revision <revision>$(tput sgr 0)"
    echo "  Git revision being deployed."
    echo
    echo "$(tput setaf 3)-g|--gitrepositoryproject <name of project containing git repo>$(tput sgr 0)"
    echo "  Name of the git project to which the git repository of the service belongs to, the default is s4."
    echo
    echo "$(tput setaf 3)-o|--portmapping <portmapping> (y/n)$(tput sgr 0)"
    echo "  Whether to enable portmapping (y/n), the default is n"
    echo
    echo "$(tput setaf 3)-e|--exposed <service exposed via api or not> (y/n)$(tput sgr 0)"
    echo "  Whether the service is exposed externally (y/n), the default is n"
    echo
    echo "$(tput setaf 3)-n|--deploymentenvironment <deployment environment> (dev/prod)$(tput sgr 0)"
    echo "  The deployment environment(development/production) (dev/prod), the default is dev"
    echo
    echo "$(tput setaf 3)-v|--volumedirectory <volume directory>$(tput sgr 0)"
    echo "  The directory under which any volume mounts will be created. If not provided, the directory specified in base compose file (docker-compose.yml) is used."
    echo
    echo "$(tput setaf 3)-v|--volumemapping <volume mapping inside container>$(tput sgr 0)"
    echo "  The directory inside the container to which the volume is mapped to, the default is /var/lib/mysql"
    echo
    echo "$(tput setaf 3)-l|--gitrepositoryurl <url of git repository of service>$(tput sgr 0)"
    echo "  The url of git repository of service"
    echo
    echo "$(tput setaf 3)[-h|help]$(tput sgr 0)"
    echo "  Print this message."
}

# Read commandline arguments:
while :; do
    case $1 in
        -h|--help)
            usage
            exit 1
            ;;
        -i|--imagenamewithtag)
            if [ -n "$2" ]; then
                IMAGE_NAME_WITH_TAG="$2"
                shift
            fi
            ;;
        -m|--imagerepository)
            if [ -n "$2" ]; then
                IMAGE_REPOSITORY="$2"
                shift
            fi
            ;;
        -u|--imagerepositoryusername)
            if [ -n "$2" ]; then
                IMAGE_REPOSITORY_USERNAME="$2"
                shift
            fi
            ;;
        -p|--imagerepositorypassword)
            if [ -n "$2" ]; then
                IMAGE_REPOSITORY_PASSWORD="$2"
                shift
            fi
            ;;
        -s|--servicename)
            if [ -n "$2" ]; then
                SERVICE_NAME="$2"
                shift
            fi
            ;;
        -d|--deploymentname)
            if [ -n "$2" ]; then
                DEPLOYMENT_NAME="$2"
                shift
            fi
            ;;
        -r|--revision)
            if [ -n "$2" ]; then
                REVISION="$2"
                shift
            fi
            ;;
        -o|--portmapping)
            if [ -n "$2" ]; then
                PORTMAPPING="$2"
                shift
            fi
            ;;
        -g|--gitrepositoryproject)
            if [ -n "$2" ]; then
                GIT_REPOSITORY_PROJECT="$2"
                shift
            fi
            ;;
        -e|--exposed)
            if [ -n "$2" ]; then
                EXPOSED="$2"
                shift
            fi
            ;;
        -n|--deploymentenvironment)
            if [ -n "$2" ]; then
                DEPLOYMENT_ENV="$2"
                shift
            fi
            ;;
        -v|--volumedirectory)
            if [ -n "$2" ]; then
                VOLUME_DIRECTORY="$2"
                shift
            fi
            ;;
        -v|--volumemapping)
            if [ -n "$2" ]; then
                VOLUME_MAPPING="$2"
                shift
            fi
            ;;
        -l|--gitrepositoryurl)
            if [ -n "$2" ]; then
                GIT_REPOSITORY_URL="$2"
                shift
            fi
            ;;
        --)              # End of all options.
            shift
            break
            ;;
        -?*)
            printf 'WARN: Unknown option (ignored): %s\n' "$1" >&2
            ;;
        *)               # Default case: No more options, so break out of the loop.
            break
    esac
    shift # past argument or value
done

if [ -z "$IMAGE_NAME_WITH_TAG" ] || [ -z "$SERVICE_NAME" ] || [ -z "$DEPLOYMENT_NAME" ] || [ -z "$REVISION" ]
then
    usage
    exit 1;
fi

#Set the default values
if [ -z "$IMAGE_REPO" ]; then 
  IMAGE_REPOSITORY="docker.alexandra.dk"
fi
if [ -z "$GIT_REPOSITORY_PROJECT" ]; then
  GIT_REPOSITORY_PROJECT="s4"
fi
if [ -z "$PORTMAPPING" ]; then
  PORTMAPPING="n"
fi
if [ -z "$EXPOSED" ]; then
  EXPOSED="n"
fi
if [ -z "$DEPLOYMENT_ENV" ]; then
  DEPLOYMENT_ENV="dev"
fi
if [ -z "$VOLUME_MAPPING" ]; then
  VOLUME_MAPPING="/var/lib/mysql"
fi

touch_log_file
echo "" >> ${LOG}
log "DEPLOY BEGIN:"

log "IMAGE_NAME_WITH_TAG=\"${IMAGE_NAME_WITH_TAG}\""
log "IMAGE_REPOSITORY=\"${IMAGE_REPOSITORY}\""

log "SERVICE_NAME=\"${SERVICE_NAME}\""
log "DEPLOYMENT_NAME=\"${DEPLOYMENT_NAME}\""
log "REVISION=\"${REVISION}\""

log "GIT_REPOSITORY_PROJECT=\"${GIT_REPOSITORY_PROJECT}\""
log "PORTMAPPING=\"${PORTMAPPING}\""
log "EXPOSED=\"${EXPOSED}\""

log "DEPLOYMENT_ENV=\"${DEPLOYMENT_ENV}\""
log "VOLUME_DIRECTORY=\"${VOLUME_DIRECTORY}\""
log "VOLUME_MAPPING=\"${VOLUME_MAPPING}\""

LOCALREPO=~/src/$SERVICE_NAME
LOCALREPO_VC_DIR=$LOCALREPO/.git

#If remote repo url provided use that, else create the repo url from other infromation
if [ -z "$GIT_REPOSITORY_URL" ]
then
    REPOSRC=ssh://git@bitbucket-server.alexandra.dk:7999/$GIT_REPOSITORY_PROJECT/$SERVICE_NAME.git
else
    REPOSRC="$GIT_REPOSITORY_URL"
fi

#Check if repo exists, if not clone
if [ ! -d $LOCALREPO_VC_DIR ]
then
    perform "git clone $REPOSRC $LOCALREPO"
fi
perform "cd $LOCALREPO"

# Update local branch
perform "git fetch origin"
perform "git reset --hard $REVISION"

# Pull image
perform "docker login -u $IMAGE_REPOSITORY_USERNAME -p $IMAGE_REPOSITORY_PASSWORD $IMAGE_REPOSITORY"
perform "docker pull $IMAGE_REPOSITORY/$IMAGE_NAME_WITH_TAG"

#Get location of deployment configuration 
if [ "$DEPLOYMENT_ENV" == "prod" ] || [ "$DEPLOYMENT_ENV" == "production" ]
then
    DEPLOY_ENV_DIRECTORY=~/src/ms-scripts/$DEPLOYMENT_NAME/production
else
    DEPLOY_ENV_DIRECTORY=~/src/ms-scripts/$DEPLOYMENT_NAME
fi

#Get internal or external deployment configuration
if [ "$EXPOSED" == "y" ] || [ "$EXPOSED" == "Y" ]
then
    perform "cp $DEPLOY_ENV_DIRECTORY/deploy.env ./"
else
    perform "cp $DEPLOY_ENV_DIRECTORY/deploy-internal.env ./deploy.env"
fi

#Generate docker-compose.deploy.yml
if [ ! -z "$VOLUME_DIRECTORY" ]
then
    log "Deploying service with volume mounted at $VOLUME_DIRECTORY/$SERVICE_NAME-data"
    perform "printf 'version: \"3.4\"\nservices:\n  $SERVICE_NAME:\n    image: $IMAGE_REPOSITORY/$IMAGE_NAME_WITH_TAG\n  $SERVICE_NAME-database:\n    volumes:\n     - $VOLUME_DIRECTORY/$SERVICE_NAME-data:$VOLUME_MAPPING\n'> docker-compose.deploy.yml"
else
    perform "printf 'version: \"3.4\"\nservices:\n  $SERVICE_NAME:\n    image: $IMAGE_REPOSITORY/$IMAGE_NAME_WITH_TAG\n'> docker-compose.deploy.yml"
fi

#Check if portmapping is enabled/disabled
#Case when portmapping is enabled
if [ "$PORTMAPPING" == "y" ] || [ "$PORTMAPPING" == "Y" ] 
then
  log "Deploying with portmapping:"
  perform "docker-compose -f docker-compose.yml -f docker-compose.portmapping.yml -f docker-compose.debug.yml -f docker-compose.fluentd-logging.yml -f docker-compose.deploy.yml up -d"
#Case when portmapping is disabled
else
  log "Deploying without portmapping:"
  perform "docker-compose -f docker-compose.yml -f docker-compose.fluentd-logging.yml -f docker-compose.deploy.yml up -d"
fi

log "DEPLOY END"

