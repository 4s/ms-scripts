#!/bin/sh
#########################
# 4S Kafka                                                                                                                                                                                    deployment script
# 
# This script deploys kafka
# harshit.mahapatra@alexandra.dk

#Log File
LOG=~/logs/kafka-deploy.log

display_usage() {
  echo "Usage: $0 "
}

touch_log_file() { 
  mkdir -p "$(dirname "$LOG")" || return; touch "$LOG";
}

log() {
  echo "$(date): $*" >> ${LOG}; tail -n1 ${LOG}
}

die() {
  log "Error during deployment, check logfile: $LOG"
  exit 2
}

perform() {
  log "$1"
  eval "$1" >> ${LOG} || die
}

#The remote repository 
REPOSRC=ssh://git@bitbucket-server.alexandra.dk:7999/s4/kafka.git
#The local repository
LOCALREPO=~/src/kafka
LOCALREPO_VC_DIR=$LOCALREPO/.git



touch_log_file
echo "" >> ${LOG}
log "KAFKA DEPLOY BEGIN:"

#Clone repo if not present, else pull
if [ ! -d $LOCALREPO_VC_DIR ]
then
    perform "git clone $REPOSRC $LOCALREPO"
    perform "cd $LOCALREPO"
else
    perform "cd $LOCALREPO"
    perform "git pull"
fi


#Build and run kafka
perform "docker-compose -f docker-compose.yml -f docker-compose.opentele3.dev.yml up --force-recreate  -d"

log "KAFKA DEPLOY END"