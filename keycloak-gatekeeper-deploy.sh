#!/bin/sh
#########################
# Keycloak Gatekeeper deployment script
#
# This deploys keycloak gatekeeper from a given source repository along with configuration present in the given source repository.

#Log File
LOG=~/logs/keycloak-gatekeeper-setup.log

display_usage() {
  echo "Usage: $0 [SERVER_URL] [REALM-NAME] [SOURCEREPO]"
}

touch_log_file() { 
  mkdir -p "$(dirname "$LOG")" || return; touch "$LOG";
}

log() {
  echo "$(date): $*" >> ${LOG}; tail -n1 ${LOG}
}

die() {
  log "Error during deployment, check logfile: $LOG"
  exit 2
}

perform() {
  log "$1"
  eval "$1" >> ${LOG} || die
}

#Check if not enough arguments
if [[  $# -le 2 ]]; then
  display_usage
  exit 1
fi

#Extract domain name from server url.
#The following checks if server url begins with http:// or https://
if [[ ${1,,} =~ ^https?://+ ]]
then
    SERVER_URL="$(echo "$1" | awk -F/ '{print $3}')"
else
    SERVER_URL=$1
fi

#The realm name
REALM_NAME=$2

#The remote repository 
REPOSRC=$3

#The local repository
LOCALREPO=~/src/keycloak-gatekeeper
LOCALREPO_VC_DIR=$LOCALREPO/.git



touch_log_file
echo "" >> ${LOG}
log "KEYCLOAK GATEKEEPER DEPLOY BEGIN:"

#Clone repo if not present, else pull
if [ ! -d $LOCALREPO_VC_DIR ]
then
    perform "git clone $REPOSRC $LOCALREPO"
    perform "cd $LOCALREPO"
else
    perform "cd $LOCALREPO"
    perform "git pull"
fi

#Get rid of local changes
perform "git checkout ."

KEYCLOAK_REALM_URL="http://$SERVER_URL/keycloak/auth/realms/$REALM_NAME"

log "Generated realm url: $KEYCLOAK_REALM_URL"

#In place replacement of configuration files, with backup saved with .bak extensions
perform "sed -i.bak \"s+keycloak_realm_url+$KEYCLOAK_REALM_URL+g\" config.yaml"

#Run the docker images
perform "docker-compose -f docker-compose.fluentd-logging.yml -f docker-compose.yml up --force-recreate  --build -d"

echo $KEYCLOAK_REALM_URL

log "KEYCLOAK GATEKEEPER DEPLOY END"

# End