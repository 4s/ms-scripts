#!/bin/bash
#########################
# 4S Deploy Internal script
#
# harshit.mahapatra@alexandra.dk

LOG=~/logs/deploy.log

display_usage() {
  echo "Usage: $0 [IMAGE-REPO] [IMAGE:TAG] [SERVICE NAME] [DEPLOYMENT NAME] [GIT-REVISION] [PORTMAPPING(y/n)]"
}

touch_log_file() { 
  mkdir -p "$(dirname "$LOG")" || return; touch "$LOG";
}

log() {
  echo "$(date): $*" >> ${LOG}; tail -n1 ${LOG}
}

die() {
  log "Error during deployment, check logfile: $LOG"
  exit 2
}

perform() {
  log "$1"
  eval "$1" >> ${LOG} || die
}

if [[  $# -le 5 ]]; then
  display_usage
  exit 1
fi

REPO=$1
IMAGE_NAME=$2
SERVICE_NAME=$3
DEPLOYMENT_NAME=$4
REVISION=$5
PORTMAPPING=$6

touch_log_file
echo "" >> ${LOG}
log "DEPLOY BEGIN: repo=$REPO, image=$IMAGE_NAME, service=$SERVICE_NAME, deployment=$DEPLOYMENT_NAME, revision=$REVISION, portmapping=$PORTMAPPING"


LOCALREPO=~/src/$SERVICE_NAME
LOCALREPO_VC_DIR=$LOCALREPO/.git
REPOSRC=ssh://git@bitbucket-server.alexandra.dk:7999/s4/$SERVICE_NAME.git

#Check if repo exists, if not clone
if [ ! -d $LOCALREPO_VC_DIR ]
then
    perform "git clone $REPOSRC $LOCALREPO"
fi
perform "cd $LOCALREPO"

# Update local branch
perform "git fetch origin"
perform "git reset --hard $REVISION"

# Pull image
perform "docker login -u dockerpuller -p OophuePhegohz2ae $REPO"
perform "docker pull $REPO/$IMAGE_NAME"

#Update service
perform "cp ~/src/ms-scripts/$DEPLOYMENT_NAME/deploy-internal.env ./deploy.env"
perform "printf 'version: \"3.4\"\nservices:\n  $SERVICE_NAME:\n    image: $REPO/$IMAGE_NAME\n' > docker-compose.deploy.yml"

#Check if portmapping is enabled/disabled
#Case when portmapping is enabled
if [ "$PORTMAPPING" == "y" ] || [ "$PORTMAPPING" == "Y" ] 
then
  log "Deploying with portmapping:"
  perform "docker-compose -f docker-compose.yml -f docker-compose.portmapping.yml -f docker-compose.debug.yml -f docker-compose.fluentd-logging.yml -f docker-compose.deploy.yml up -d"
#Case when portmapping is disabled
else
  log "Deploying without portmapping:"
  log "Remote debugging is disabled"
  perform "docker-compose -f docker-compose.yml -f docker-compose.fluentd-logging.yml -f docker-compose.deploy.yml up -d"
fi

log "DEPLOY END"

