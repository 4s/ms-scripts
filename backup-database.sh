#!/bin/bash
#########################
# Database Backup Script
#
# harshit.mahapatra@alexandra.dk

LOG=~/logs/postgres-backup.log

touch_log_file() { 
  mkdir -p "$(dirname "$LOG")" || return; touch "$LOG";
}

update_suffix_file() { 
  mkdir -p "$(dirname "$SUFFIX_FILE")" || return; echo "$SUFFIX">"$SUFFIX_FILE";
}

log() {
  echo "$(date): $*" >> ${LOG}; tail -n1 ${LOG}
}

die() {
  log "Error during deployment, check logfile: $LOG"
  exit 2
}

perform() {
  log "$1"
  eval "$1" >> ${LOG} || die
}

function usage()
{
    echo "Backup a postgres database"
    echo
    echo "$(tput setaf 3)Usage:$(tput sgr 0)"
    echo "backup-database.sh [-t|--databasetype <databasetype>] [-c|--containername <containername>] [-b|--backupdirectory <backup-directory>] [-l|--limit <backup-limit>] [-s|--suffix <backup-suffix>] [-d|--databasename <databasename>] [-u|--username <database-username>] [-p|--password <database-password>] [-h|help]"
    echo
    echo "$(tput setaf 3)Options:$(tput sgr 0)"
    echo
    echo "$(tput setaf 3)[-t|--databasetype <databasetype>]$(tput sgr 0)"
    echo "  Type of database to back up, defaults to mariadb."
    echo
    echo "$(tput setaf 3)[-c|--containername <containername>]$(tput sgr 0)"
    echo "  Name of container running the postgres database."
    echo
    echo "$(tput setaf 3)-b|--backupdirectory <backup-directory>$(tput sgr 0)"
    echo "  Directory to store the backups"
    echo
    echo "$(tput setaf 3)-l|--limit <backup-limit>$(tput sgr 0)"
    echo "  Limit on the number of backups. Default is 3."
    echo
    echo "$(tput setaf 3)-s|--suffix <backup-suffix>$(tput sgr 0)"
    echo "  Suffix to append to backup. Default is 1."
    echo
    echo "$(tput setaf 3)-d|--databasename <databasename>$(tput sgr 0)"
    echo "  Name of database to backup."
    echo
    echo "$(tput setaf 3)-u|--username <database-username>$(tput sgr 0)"
    echo "  Username for accessing the database."
    echo
    echo "$(tput setaf 3)-p|--password <database-password>$(tput sgr 0)"
    echo "  Password for accessing the database."
    echo
    echo "$(tput setaf 3)[-h|help]$(tput sgr 0)"
    echo "  Print this message."
}

# Read commandline arguments:
while :; do
    case $1 in
        -h|--help)
            usage
            exit 1
            ;;
        -t|--databasetype)
            if [ -n "$2" ]; then
                DATABASE_TYPE="$2"
                shift
            fi
            ;;
        -c|--containername)
            if [ -n "$2" ]; then
                CONTAINER_NAME="$2"
                shift
            fi
            ;;
        -b|--backupdirectory)
            if [ -n "$2" ]; then
                BACKUP_DIRECTORY="$2"
                shift
            fi
            ;;
        -l|--limit)
            if [ -n "$2" ]; then
                BACKUP_LIMIT="$2"
                shift
            fi
            ;;
        -s|--suffix)
            if [ -n "$2" ]; then
                SUFFIX="$2"
                shift
            fi
            ;;
        -d|--databasename)
            if [ -n "$2" ]; then
                DATABASE_NAME="$2"
                shift
            fi
            ;;
        -u|--username)
            if [ -n "$2" ]; then
                USERNAME="$2"
                shift
            fi
            ;;
        -p|--password)
            if [ -n "$2" ]; then
                PASSWORD="$2"
                shift
            fi
            ;;
        --)              # End of all options.
            shift
            break
            ;;
        -?*)
            printf 'WARN: Unknown option (ignored): %s\n' "$1" >&2
            ;;
        *)               # Default case: No more options, so break out of the loop.
            break
    esac
    shift # past argument or value
done

if [ -z "$CONTAINER_NAME" ] || [ -z "$BACKUP_DIRECTORY" ]
then
    usage
    exit 1;
fi

SUFFIX_FILE=$BACKUP_DIRECTORY/lastsuffix.log

#Set the default values
if [ -z "$DATABASE_TYPE" ]; then
    DATABASE_TYPE=mariadb
fi
if [ -z "$SUFFIX" ]; then
    BACKUP_LIMIT=3
fi

#Suffix file is used to store the backup number which is used to keep track of and limit the number of backups.
if [ -z "$SUFFIX" ]; then
  if [ ! -f "$SUFFIX_FILE" ]; then
    log "SUFFIX NOT PROVIDED AND SUFFIX_FILE NOT FOUND, SUFFIX IS DEFAULTING TO 1."
    SUFFIX=1
    update_suffix_file
  else
    LAST_SUFFIX=$(cat "$SUFFIX_FILE")
    if [ -n "$LAST_SUFFIX" ] && [ "$LAST_SUFFIX" -eq "$LAST_SUFFIX" ] 2>/dev/null; then
        SUFFIX=$(($LAST_SUFFIX + 1))
    else
        log "LAST SUFFIX NOT A NUMBER, CURRENT SUFFIX IS DEFAULTING TO 1."
        SUFFIX=1
    fi
    if [ $SUFFIX -gt $BACKUP_LIMIT ]; then
        log "BACKUP LIMIT REACHED, SUFFIX DEFAULTING TO 1"
        SUFFIX=1
    fi
  fi
fi

BACKUP_FILE=${BACKUP_DIRECTORY}/${CONTAINER_NAME}_backup_${SUFFIX}.sql

log "DATABASE_TYPE: $DATABASE_TYPE"
log "CONTAINER_NAME: $CONTAINER_NAME"
log "BACKUP_DIRECTORY: $BACKUP_DIRECTORY"
log "BACKUP_FILE: $BACKUP_FILE"
log "BACKUP_LIMIT: $BACKUP_LIMIT"
log "SUFFIX: $SUFFIX"
log "DATABASE_NAME: $DATABASE_NAME"
log "USERNAME: $USERNAME"

touch_log_file
echo "" >> ${LOG}
log "BACKUP PROCESS BEGIN:"


shopt -s nocasematch
case "$DATABASE_TYPE" in
 mariadb )
  log "BACKING UP MARIADB DATABASE:"
  perform "docker exec $CONTAINER_NAME mysqldump -u $USERNAME -p$PASSWORD --single-transaction --skip-lock-tables $DATABASE_NAME > $BACKUP_FILE"
  ;;
 postgres ) 
 log "BACKING UP POSTGRES DATABASE:"
 perform "docker exec -t $CONTAINER_NAME  pg_dumpall -c -U $USERNAME --database $DATABASE_NAME  > $BACKUP_FILE"
 ;;
 *) printf 'WARN: Unknown option (ignored): %s\n' "$1" >&2;;
esac

log "DATABASE BACKUP SUCCESSFUL."
log "UPDATING SUFFIX FILE:"
update_suffix_file
log "BACKUP PROCESS END"

