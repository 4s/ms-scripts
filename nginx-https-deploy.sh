#!/bin/sh
#########################
# 4S Nginx https deployment script
#
# This script creates https nginx configuration for a given server url using let's encrypt and deploys nginx.
# harshit.mahapatra@alexandra.dk

#Log File
LOG=~/logs/nginx-https-setup.log

display_usage() {
  echo "Usage: $0 [SERVER-URL] [LETSENCRYPT_EMAIL] [LETSENCRYPT_STAGING] [REPLACE_CERTIFICATES] [OPTIONAL:SOURCEREPO]"
}

touch_log_file() { 
  mkdir -p "$(dirname "$LOG")" || return; touch "$LOG";
}

log() {
  echo "$(date): $*" >> ${LOG}; tail -n1 ${LOG}
}

die() {
  log "Error during deployment, check logfile: $LOG"
  exit 2
}

perform() {
  log "$1"
  eval "$1" >> ${LOG} || die
}

#Check if not enough arguments
if [[  $# -le 2 ]]; then
  display_usage
  exit 1
fi

#Extract domain name from server url.
#The following checks if server url begins with http:// or https://
if [[ ${1,,} =~ ^https?://+ ]]
then
    SERVER_URL="$(echo "$1" | awk -F/ '{print $3}')"
else
    SERVER_URL=$1
fi

#Let's encrypt variables
#Email used for issuing certificate
email=$2
#Whether to use test (0)/ production (1) version of lets encrypt certificates.
#Test certificates are good for testing the configuration and do not count in the letsencrypt rate limit, but are not recognized as legitimate by the browsers.
staging=$3
#Whether to replace existing certificates
replace_certificates=$4

#The remote repository 
REPOSRC=${5:-"ssh://git@bitbucket-server.alexandra.dk:7999/s4/nginx.git"}

#The local repository
LOCALREPO=~/src/nginx
LOCALREPO_VC_DIR=$LOCALREPO/.git



touch_log_file
echo "" >> ${LOG}
log "NGINX DEPLOY BEGIN:"

#Clone repo if not present, else pull
if [ ! -d $LOCALREPO_VC_DIR ]
then
    perform "git clone $REPOSRC $LOCALREPO"
    perform "cd $LOCALREPO"
else
    perform "cd $LOCALREPO"
    perform "git reset --hard"
    perform "git pull"
fi


#In place replacement of configuration files, with backup saved with .bak extensions
perform "sed -i.bak \"s+server_url+$SERVER_URL+g\" service_https.conf"

#RSA Key size for the dummy and data path to store certificates
rsa_key_size=4096
data_path="certbot"

#Deploy and exit if certificates exist and replacement is false
if [ -d "$data_path" ]; then
    perform "echo \"Existing data found...\""
  if [ "$replace_certificates" != "y" ] && [ "$replace_certificates" != "Y" ]; then
    perform "docker-compose -f docker-compose.yml -f docker-compose.fluentd-logging.yml -f docker-compose.https.yml up --force-recreate  -d"
    perform "echo \"Exiting\""
    log "NGINX SETUP END"
    exit
  else
    perform "echo \"Replacing exisitng certificates\""  
  fi
fi


#Download the recommended TLS parameters for certbot
if [ ! -e "$data_path/conf/options-ssl-nginx.conf" ] || [ ! -e "$data_path/conf/ssl-dhparams.pem" ]; then
  perform "echo \"### Downloading recommended TLS parameters ...\""
  perform "mkdir -p \"$data_path/conf\""
  perform "curl -s https://raw.githubusercontent.com/certbot/certbot/master/certbot-nginx/certbot_nginx/_internal/tls_configs/options-ssl-nginx.conf > \"$data_path/conf/options-ssl-nginx.conf\""
  perform "curl -s https://raw.githubusercontent.com/certbot/certbot/master/certbot/certbot/ssl-dhparams.pem > \"$data_path/conf/ssl-dhparams.pem\""
  echo
fi



#Create dummy (self-signed) certificates to start nginx 
perform "echo \"### Creating dummy certificate for $SERVER_URL ...\""
path="/etc/letsencrypt/live/$SERVER_URL"
perform "mkdir -p \"$data_path/conf/live/$SERVER_URL\""
perform "docker-compose -f docker-compose.yml -f docker-compose.fluentd-logging.yml -f docker-compose.https.yml run --rm --entrypoint \"\
  openssl req -x509 -nodes -newkey rsa:1024 -days 1\
    -keyout '$path/privkey.pem' \
    -out '$path/fullchain.pem' \
    -subj '/CN=localhost'\" certbot"

perform "echo \"### Starting nginx ...\""
perform "docker-compose -f docker-compose.yml -f docker-compose.https.yml -f docker-compose.fluentd-logging.yml up --force-recreate -d nginx"

#Once nginx has started, delete the dummy (self-signed) certificates
perform "echo \"### Deleting dummy certificate for $SERVER_URL ...\""
perform "docker-compose -f docker-compose.yml -f docker-compose.fluentd-logging.yml -f docker-compose.https.yml run --rm --entrypoint \"\
  rm -Rf /etc/letsencrypt/live/$SERVER_URL && \
  rm -Rf /etc/letsencrypt/archive/$SERVER_URL && \
  rm -Rf /etc/letsencrypt/renewal/$SERVER_URL.conf\" certbot"

#Request let's encrypt for certificate using http-01 challenge
perform "echo \"### Requesting Let's Encrypt certificate for $SERVER_URL ...\""

#Prepare arguments for certbot
#Join $SERVER_URL to -d args
domain_args=""
for domain in "${SERVER_URL[@]}"; do
  domain_args="$domain_args -d $domain"
done

# Select appropriate email arg
case "$email" in
  "") email_arg="--register-unsafely-without-email" ;;
  *) email_arg="--email $email" ;;
esac

# Enable staging mode if needed
if [ $staging != "0" ]; then staging_arg="--staging"; fi

# Finally request let's encrypt using prepared arguments  
perform "docker-compose -f docker-compose.yml -f docker-compose.fluentd-logging.yml -f docker-compose.https.yml run --rm --entrypoint \"\
  certbot certonly --webroot -w /var/www/certbot \
    $staging_arg \
    $email_arg \
    $domain_args \
    --no-eff-email \
    --rsa-key-size $rsa_key_size \
    --agree-tos \
    --force-renewal\" certbot"


#Reload nginx after getting the certificates from let's encrypt
perform "echo \"### Reloading nginx ...\""
perform "docker-compose -f docker-compose.yml -f docker-compose.fluentd-logging.yml -f docker-compose.https.yml exec -T nginx nginx -s reload"

log "NGINX SETUP END"

# End