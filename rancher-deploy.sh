#!/bin/bash
#########################
# 4S Deploy Rancher script
#
# harshit.mahapatra@alexandra.dk

LOG=~/logs/deploy.log

display_usage() {
  echo "Usage: $0 [SERVER-URL] [IMAGE:TAG]"
}

touch_log_file() { 
  mkdir -p "$(dirname "$LOG")" || return; touch "$LOG";
}

log() {
  echo "$(date): $*" >> ${LOG}; tail -n1 ${LOG}
}

die() {
  log "Error during deployment, check logfile: $LOG"
  exit 2
}

perform() {
  log "$1"
  eval "$1" >> ${LOG} || die
}

if [[  $# -ne 2 ]]; then
  display_usage
  exit 1
fi

#Extract domain name from server url.
#The following checks if server url begins with http:// or https://
if [[ ${1,,} =~ ^https?://+ ]]
then
    SERVER_URL="$(echo "$1" | awk -F/ '{print $3}')"
else
    SERVER_URL=$1
fi

#The version of rancher
IMAGE_TAG=$2

REPO=rancher
IMAGE_NAME=rancher

touch_log_file
echo "" >> ${LOG}
log "DEPLOY BEGIN: image_tag=$IMAGE_TAG, server-url=$SERVER_URL, image=$REPO/$IMAGE_NAME:$IMAGE_TAG"


# Pull image
perform "docker pull $REPO/$IMAGE_NAME:$IMAGE_TAG"

#Run image
perform "
  docker run -d --restart=unless-stopped \
  -p 80:80 -p 443:443 \
  -v ~/rancher:/var/lib/rancher \
  --name rancher-server \
  --privileged \
  $REPO/$IMAGE_NAME:$IMAGE_TAG \
  --acme-domain $SERVER_URL
"

log "DEPLOY END"

